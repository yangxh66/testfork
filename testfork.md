# 区别
fork()与vfock()都是创建一个进程;
1. fork():子进程拷贝父进程的数据段，代码段
2. vfork(): 子进程与父进程共享数据段
3. fork()父子进程的执行次序不确定，vfork 保证子进程先运行，在调用exec 或exit 之前与父进程数据是共享的,在它调用exec或exit 之后父进程才可能被调度运行。
vfork()保证子进程先运行，在她调用exec 或exit 之后父进程才可能被调度运行。如果在调用这两个函数之前子进程依赖于父进程的进一步动作，则会导致死锁。

## fork
fork()函数用于从已存在的进程中创建一个新的进程，新的进程称为子进程，而原进程称为父进程，fork()的返回值有两个，子进程返回0，父进程返回子进程的进程号，进程号都是非零的正整数，所以父进程返回的值一定大于零，在pid=fork();语句之前只有父进程在运行，而在pid=fork();之后，父进程和新创建的子进程都在运行，所以如果pid==0，那么肯定是子进程，若pid ！=0 （事实上肯定大于0），那么是父进程在运行。
```python
[root@vm1 testfork]# ./testfork
cnt=1
cnt=1
child process ID: 126624
parent process ID: 126623
[root@vm1 testfork]#
```
fork()函数子进程拷贝父进程的数据段代码段，所以
cnt++;
printf("cnt= %d\n",cnt);
将被父子进程各执行一次，但是子进程执行时使自己的数据段里面的（这个数据段是从父进程那copy 过来的一模一样）count+1，同样父进程执行时使自己的数据段里面的count+1，他们互不影响.
## vfork
vfork 保证子进程先运行,在子进程调用exec 或exit之前与父进程数据是共享的,所以子进程退出后把父进程的数据段count改成1 了，子进程退出后，父进程又执行，最终就将count变成了2
```java
[root@vm1 testfork]# ./testvfork
cnt=1
child process ID: 138961
cnt=2
parent process ID: 138960
[root@vm1 testfork]#
```
